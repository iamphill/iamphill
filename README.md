## Hello world :wave:

My name is Phil Hughes and I currently live in Liverpool, UK.
I have a wife, Elizabeth, and 2 girls, Emilia and Florence.
We also have a dog, Milo.

I've worked at GitLab since 2016, whilst also taking a few months break
in the middle to get over some burnout. Currently I am a Staff
Fullstack Engineer on the Code Review team working on all parts diffs
though I do love to improve other parts of the app.

Throughout my time at GitLab I have achieved a lot of achievements
that I am proud of, from introducing VueJS to our codebase, creating
the first frontend version of issue boards to creating the Web IDE.

### How do I work? :computer:

I work best of early mornings. It is the time when I am most productive.
Normally my day will start with code reviews which could, depending how
many I have to review, could take a few hours. Personally I find it
most important to unblock others first as I know the pain of waiting
for someone else before I can do my work.

After reviews I will get to my own work starting with resolving review
comments to then working on new issues. If I have no new issues, I will
normally go on a bug hunt and try to find bugs to quickly fix.

Regressions aren't nice, so I will always prioritise these getting fixed
first. Even if not many users will notice the regression it is still
important these get fixed first.

### Meetings? :speech_balloon:

I don't do well in group meetings :sweat_smile: I am exteremly introverted
and struggle to raise my voice in group settings. If I am in a meeting 
as a group I prefer other people calling me out to raise my voice, this
way I feel I can contribute without having to interupt others.

1:1 meetings are much better for me. Talking 1:1 with someone is something
I find much easier, I find it easier to connect with people this way.
So feel free to schedule a call with me to chat!

### Knowledge of GitLab :question:

Over my time at GitLab I have gained a large amount of knowledge about
various ways things are done, especially on the frontend. This is something
I need to work on giving back others, if you have questions, feel free
to just send them my way.

### Around the web :earth_africa:

I try to remain quiet on the internet :sweat_smile: but if you want
to follow me places:

- [Twitter](https://twitter.com/iamphill)
- [Reddit](https://www.reddit.com/user/iamphill)
- [GitHub](https://github.com/iamphill)
